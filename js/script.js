//                   ДР №1
//                  Теория
// 1.
// С помошью ключевых слов "let", "var" и "const"
// let userName = 'Денис';
// var color = 'Красный'; (устаревшее)
// const myAge = '35';

// 2.
// Метод prompt (функция для получения данных от пользователя) выводит
// сообщение и ждёт, пока пользователь введёт текст, а затем возвращает
// введённое значение или null, если ввод отменён (CANCEL / Esc).

// Метод confirm (функция для получения от пользователя ("Да" / "Нет")
// выводит сообщение и ждёт, пока пользователь нажмёт «OK» или «CANCEL»
// и возвращает true / false

// 3.
// Не явное преобразование типов - это автоматическая конвертация значений
// между различных типов для корректного выполнения программы.
// let result = 36 - 12 / "4";
// console.log(result);

//                    Практика

// 1.
let admin = 'Роджер';
let name = 'Денис';
admin = name;
console.log(admin);

// 2.
let days, sec;
days = 7;
sec = days * 24 * 60 * 60
alert(`7 days=${sec}s`);
console.log(7 + ' days = ' + sec + ' s');

// 3.
let age;
age = prompt('How old are you ?');
alert(`You are ${age} years old`);
console.log('You are ' + age + ' years old');

